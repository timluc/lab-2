import java.util.Scanner;
public class PartThree {
  public static void main(String[] args) {
  Scanner read = new Scanner(System.in);
  // System.out.print(" ");  
  System.out.println("Please enter the length of the square.");
  double side1 = read.nextDouble();  
  //System.out.println(side1);
  System.out.println("Please enter the length of the rectangle.");
  double side2 = read.nextDouble();  
  System.out.println("Please enter the width of the square.");
  double side3 = read.nextDouble();
  System.out.print("The Area of the square is ");
  System.out.println(AreaComputations.areaSquare(side1));
  AreaComputations ac = new AreaComputations();
  System.out.print("The Area of the rectangle is ");
  System.out.println(ac.areaRectangle(side2,side3));
  }
}
